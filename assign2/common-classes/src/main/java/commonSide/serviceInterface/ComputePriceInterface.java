package commonSide.serviceInterface;

import commonSide.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by cata_ on 11/15/2017.
 */
public interface ComputePriceInterface extends Remote {
    double computeSelling(Car c) throws RemoteException;
}
