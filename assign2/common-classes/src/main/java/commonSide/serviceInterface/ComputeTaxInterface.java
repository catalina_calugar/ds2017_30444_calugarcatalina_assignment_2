package commonSide.serviceInterface;

import commonSide.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by cata_ on 11/7/2017.
 */
public interface ComputeTaxInterface extends Remote {

    double computeTax(Car c) throws RemoteException;

}