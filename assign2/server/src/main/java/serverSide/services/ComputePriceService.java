package serverSide.services;

import commonSide.entities.Car;
import commonSide.serviceInterface.ComputePriceInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by cata_ on 11/15/2017.
 */
public class ComputePriceService  extends UnicastRemoteObject implements ComputePriceInterface {

    public ComputePriceService() throws RemoteException {
        super();
    }

    @Override
    public double computeSelling(Car c) throws RemoteException {

        if (c.getPrice() <= 0) {
            throw new IllegalArgumentException("Price must be positive.");
        }

        double priceSelling = 0;
        priceSelling = c.getPrice() - (c.getPrice()/7)*(2015 - c.getYear());
        return priceSelling;
    }

}
