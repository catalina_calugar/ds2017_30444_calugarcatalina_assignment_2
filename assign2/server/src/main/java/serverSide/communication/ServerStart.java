package serverSide.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import serverSide.services.ComputePriceService;
import serverSide.services.ComputeTaxService;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

@SuppressWarnings("deprecation")
public class ServerStart {
	private static final Log LOGGER = LogFactory.getLog(ServerStart.class);

	private static final int PORT = 1099;

	private ServerStart() {
	}

	public static void main(String[] args) {
		try {
            LocateRegistry.createRegistry(PORT);
			ComputeTaxService computeTaxService = new ComputeTaxService();
			ComputePriceService priceService = new ComputePriceService();

			Naming.rebind("rmi://localhost:"+PORT+"/TaxService", computeTaxService);
			Naming.rebind("rmi://localhost:"+PORT+"/ComputePriceService", priceService);

			System.err.println("Server started");
		} catch (Exception e) {
			System.err.println("Server exception: " + e.getMessage());
		}
	}
}
