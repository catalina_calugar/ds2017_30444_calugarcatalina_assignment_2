package clientSide.ui;

import commonSide.entities.Car;
import commonSide.serviceInterface.ComputePriceInterface;
import commonSide.serviceInterface.ComputeTaxInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by cata_ on 11/8/2017.
 */
public class ClientUI {
    private JFrame frame;
    private JPanel listPane;
    private JButton taxButton;
    private JButton priceButton;
    private JTextField yearField;
    private JTextField engineSizeField;
    private JTextField priceField;
    private JTextArea resultField;

    public ClientUI(ComputeTaxInterface taxService, ComputePriceInterface priceInterface) {
        frame = new JFrame("Assignment 2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(500,500));

        listPane = new JPanel();
        listPane.setLayout(new GridLayout(13,1));
        listPane.setPreferredSize( new Dimension(500, 500));

        taxButton = new JButton("Compute TAX for car");
        taxButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if((!yearField.getText().equals("")) && (!engineSizeField.getText().equals("")) && (!priceField.equals("")) ) {
                    try {
                        Car c = new Car();
                        c.setYear(Integer.parseInt(yearField.getText()));
                        c.setEngineCapacity(Integer.parseInt(engineSizeField.getText()));
                        c.setPrice(Double.parseDouble(priceField.getText()));
                        resultField.setText("Tax value: " + taxService.computeTax(c));
                    }catch (Exception ex){
                        resultField.setText("Input must be a number!");
                    }
                }else{
                    resultField.setText("All fields must be completed!");
                }
            }
        });

        priceButton = new JButton("Compute selling price");
        priceButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if((!yearField.getText().equals("")) && (!engineSizeField.getText().equals("")) && (!priceField.equals("")) ) {
                    try {
                        Car c = new Car();
                        c.setYear(Integer.parseInt(yearField.getText()));
                        c.setEngineCapacity(Integer.parseInt(engineSizeField.getText()));
                        c.setPrice(Double.parseDouble(priceField.getText()));
                        resultField.setText("Selling price for input car is: " + priceInterface.computeSelling(c));
                    }catch (Exception ex){
                        resultField.setText("Input must be a number!");
                    }
                }else{
                    resultField.setText("All fields must be completed!");
                }
            }
        });

        yearField = new JTextField();
        yearField.setPreferredSize( new Dimension( 100, 14 ) );
        engineSizeField = new JTextField();
        engineSizeField.setPreferredSize( new Dimension( 100, 14 ) );
        priceField = new JTextField();
        priceField.setPreferredSize( new Dimension( 100, 14 ) );

        resultField = new JTextArea();
        resultField.setPreferredSize( new Dimension( 100, 100 ) );

        listPane.add(new JLabel("  Add car data"));
        listPane.add(new JLabel("  Year: "));
        listPane.add(yearField);
        listPane.add(new JLabel("  Engine size: "));
        listPane.add(engineSizeField);
        listPane.add(new JLabel("  Price: "));
        listPane.add(priceField);
        listPane.add(new JLabel("   "));
        listPane.add(taxButton);
        listPane.add(priceButton);
        listPane.add(new JLabel("  Result:  "));
        listPane.add(resultField);

        listPane.setVisible(true);
        frame.add(listPane);
        frame.pack();
        frame.setVisible(true);
    }

}
