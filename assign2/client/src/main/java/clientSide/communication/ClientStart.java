package clientSide.communication;

import clientSide.ui.ClientUI;
import commonSide.entities.Car;
import commonSide.serviceInterface.ComputePriceInterface;
import commonSide.serviceInterface.ComputeTaxInterface;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

@SuppressWarnings("deprecation")
public class ClientStart {
	private static final Log LOGGER = LogFactory.getLog(ClientStart.class);
	private static final int PORT = 1099;

	private static ComputeTaxInterface taxService;
	private static ComputePriceInterface priceService;

	private ClientStart() {
	}

	public static void main(String[] args) throws
            MalformedURLException, RemoteException, NotBoundException {
		
		try {

            taxService = (ComputeTaxInterface) Naming.lookup("rmi://localhost:"+PORT+"/TaxService");
			priceService = (ComputePriceInterface) Naming.lookup("rmi://localhost:"+PORT+"/ComputePriceService");

            new ClientUI(taxService, priceService);

			System.out.println("Tax value: " + taxService.computeTax(new Car(2009, 2000, 1000)));
            System.out.println("Selling price: " + priceService.computeSelling(new Car(2009, 2000, 1000)));
			
		} catch (Exception e) {
			LOGGER.error("",e);
		}
	}
}
